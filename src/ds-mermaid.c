#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <assert.h>

#define MAX_JOBS 500
#define MAX_SERVERS 1000
#define MAX_SERVER_TYPE_LENGTH 128
#define MAX_LINE_LENGTH 1024
#define MAX_EVENT_LENGTH 20
#define DS_SIM_HEADER	"# ds-sim server"
#define UNKNOWN -1
#define TRUE	1
#define FALSE 	0


typedef struct {
    int id;
	int submit_time;
    int start_time;
    int end_time;
} Job;

typedef struct {
    char type[MAX_SERVER_TYPE_LENGTH];
	int id;
    Job jobs[MAX_JOBS];
    int job_count;
} Server;

void trim(char *str) {
    char *end;
    while(isspace((unsigned char)*str)) str++;
    if(*str == 0) return;
    end = str + strlen(str) - 1;
    while(end > str && isspace((unsigned char)*end)) end--;
    end[1] = '\0';
}

int compare_servers(const void *a, const void *b) {
	int ret;
    if (ret = strcmp(((Server*)a)->type, ((Server*)b)->type))
		return ret;
	return ((Server*)a)->id > ((Server*)b)->id;
}

int skip_headers(FILE *file, char **line) {
	int num_fields, time;

	fgets(*line, MAX_LINE_LENGTH, file);
	if (strncmp(*line, DS_SIM_HEADER, strlen(DS_SIM_HEADER))) {
		fprintf(stderr, "Invalid input file!\n");
		return FALSE;
	}
	while (fgets(*line, MAX_LINE_LENGTH, file)) {
		
		if ((num_fields = sscanf(*line, "t %d:", &time)) == 1)
			break;
	}
	
	return (num_fields ? TRUE : FALSE);
}

int get_server_index(Server servers[], int server_count, const char *server_type, const int server_id) {
    for (int i = 0; i < server_count; i++) {
        if (strcmp(servers[i].type, server_type) == 0 && servers[i].id == server_id) {
            return i;
        }
    }
    return UNKNOWN;
}

int parse_log(FILE *file, Server servers[], int *server_count) {
    char *line = (char *) malloc(sizeof(char) * MAX_LINE_LENGTH);
    char event[MAX_EVENT_LENGTH], server_type[MAX_SERVER_TYPE_LENGTH];
    int time, total_job_count = 0, job_id, server_id, cpu, mem, disk, runtime;
	
	if (!skip_headers(file, &line)) {
		free(line);
		return FALSE;
	}

    do {
		 if (sscanf(line, "t %d: job %d -- CPU: %d, Mem: %d, Disk: %d, Est Runtime: %d", &time, &job_id, &cpu, &mem, &disk, &runtime) == 6 ||
			sscanf(line, " job %d -- CPU: %d, Mem: %d, Disk: %d, Est Runtime: %d", &job_id, &cpu, &mem, &disk, &runtime) == 5) {
            total_job_count++;
			if (total_job_count > MAX_JOBS) {
				fprintf(stderr, "Maximum #jobs (%d) exceeded!\n", MAX_JOBS);
				return FALSE;
			}
        }
		else if (sscanf(line, " job %d (waiting) on %s %d (booting) SCHEDULED", &job_id, server_type, &server_id) == 3 ||
				sscanf(line, " job %d (running) on %s %d (active) SCHEDULED", &job_id, server_type, &server_id) == 3 ) {
			trim(server_type);
            int server_index = get_server_index(servers, *server_count, server_type, server_id);
            if (server_index == UNKNOWN) {
                server_index = *server_count;
				strcpy(servers[*server_count].type, server_type);
				servers[*server_count].id = server_id;
                servers[*server_count].job_count = 0;
                (*server_count)++;
				if (*server_count > MAX_SERVERS) {
					fprintf(stderr, "Maximum #servers (%d) exceeded!\n", MAX_SERVERS);
					return FALSE;
				}
            }
			int job_index = servers[server_index].job_count;
			servers[server_index].jobs[job_index].id = job_id;
			servers[server_index].jobs[job_index].submit_time = time;
			servers[server_index].job_count++;
		}
		else if (sscanf(line, "t %d: job %d on %s %d %s", &time, &job_id, server_type, &server_id, event) == 5 ||
				sscanf(line, " job %d on %s %d %s", &job_id, server_type, &server_id, event) == 4) {
            int server_index = get_server_index(servers, *server_count, server_type, server_id);
			assert(server_index != UNKNOWN);
			for (int i = 0; i < servers[server_index].job_count; i++) {
				if (servers[server_index].jobs[i].id == job_id) {
					if (strcmp(event, "RUNNING") == 0)
						servers[server_index].jobs[i].start_time = time;
					else if (strcmp(event, "COMPLETED") == 0)
						servers[server_index].jobs[i].end_time = time;
					break;
				}
			}
        }
    }
	while (fgets(line, MAX_LINE_LENGTH, file));
	
	free(line);
	
	return TRUE;
}

void generate_mermaid_gantt(Server servers[], int server_count) {
    printf("gantt\n");
    printf("    title ds-sim Job Scheduling Gantt Chart\n");
    printf("    dateFormat X\n");
    printf("    axisFormat %%s\n\n");

    for (int i = 0; i < server_count; i++) {
        printf("    section %s %d\n", servers[i].type, servers[i].id);
        for (int j = 0; j < servers[i].job_count; j++) {
            Job *job = &servers[i].jobs[j];
			if (job->start_time > job->submit_time)
				printf("    Job %d Waiting: crit, %d, %d\n", job->id, job->submit_time, job->start_time);
            printf("    Job %d: %d, %d\n", job->id, job->start_time, job->end_time);
        }
        printf("\n");
    }
}

int main(int argc, char **argv) {
    Server servers[MAX_SERVERS];
    int server_count = 0;
	FILE *file;

	if (argc < 2) {
		fprintf(stderr, "Usage: %s ds-sim_simulation_log_file\n", argv[0]);
		exit(1);
	}
	
    file = fopen(argv[1], "r");
    if (file == NULL) {
        perror("Error opening file");
        return 1;
    }
	if (parse_log(file, servers, &server_count)) {
		qsort(servers, server_count, sizeof(Server), compare_servers);
		generate_mermaid_gantt(servers, server_count);
	}
	
	fclose(file);
	
    return 0;
}