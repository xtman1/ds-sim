import os
import re
import fnmatch
import random
import time
import argparse
import numpy


#lognormal parameters of time distribution from teragrid
teragrid_time_p = {
     'mean': 0.688,
     'stdev':0.531}
teragrid_node_p = {
     'mean': -1.408,
     'stdev':1.907,
     'failure_ratio': 0.418}


#lognormal parameters of time distribution from nd07cpu
nd07cpu_time_p = {
     'mean': 2.442,
     'stdev':0.661}
nd07cpu_node_p = {
     'mean': 1.433,
     'stdev':2.022,
     'failure_ratio': 0.982}


#lognormal parameters of time distribution from websites
websites02_time_p = {
     'mean': 2.43,
     'stdev':0.30}
websites02_node_p = {
     'mean': 1.65,
     'stdev':1.22,
     'failure_ratio': 1.0}


def get_node_num_server_num(xml_file_loc):
    re_pattern_server_types = re.compile(r'''<server.+type="(.+?)" .+/>''')
    re_pattern_servers = re.compile(r'''<server.+limit="(.+?)" .+/>''')
    re_pattern_endtime = re.compile(r'''<condition.+type="endtime".+value="(.+?)".+/>''')
    server_dict = dict()
    with open(xml_file_loc) as f:
        contents = f.read()
        re_server_types = re.findall(re_pattern_server_types, contents)
        re_servers = [int(i) for i in re.findall(re_pattern_servers, contents)]
        total_servers = sum(re_servers)

        re_endtime = re.findall(re_pattern_endtime, contents)
        total_time = sum([int(s) for s in re_endtime])
        server_index = 0
        for i, s in enumerate(re_servers):
            for sub_i in range(s):
                server_dict[server_index] = (re_server_types[i], sub_i)
                server_index += 1
    return total_servers, total_time, server_dict


def get_top_n(howmanay, dist):
    dist_list = list(dist)
    r = list()
    for i in range(howmanay):
        if len(dist_list) == 0:
            break
        max_value = max(dist_list)
        max_index = dist_list.index(max_value)
        r.append(max_index)
        # print(max_value, max_index)
        dist_list[max_index] = 0
    return r


def get_lognormal_time_distribution(p, total_time_or_node):
    numpy_r = numpy.random.lognormal(p['mean'], p['stdev'], total_time_or_node)
    mean = numpy.mean(numpy_r)
    stdev = numpy.std(numpy_r)
    return mean, stdev, numpy_r


def pre_generate_lognormal_time_distribution(p, total_time_or_node, total_servers):
    result_list = list()
    time_stamp = time.time()
    max_pre_set_numbers = int(total_servers/10)
    if max_pre_set_numbers < 10:
        max_pre_set_numbers = 10
    print(f'Making a set (total: {max_pre_set_numbers}) of time distribution for nodes has started. (This is a long running process)')
    for i in range(max_pre_set_numbers):
        # print(f'generating pre normal seq: {i}')
        numpy_r = numpy.random.lognormal(p['mean'], p['stdev'], total_time_or_node)
        result_list.append(get_top_n(len(numpy_r), numpy_r))
    print(f'Making a set of time distribution of node has finished: took ({int(time.time()-time_stamp)}) seconds')
    return result_list


def make_failure_trace_like_fta_format(server_dict, node_d_dict):
    result = dict()
    for node_id, failure_times in node_d_dict.items():
        while True:
            ft_duration = list()

            if len(failure_times) == 0:
                break
            ft = failure_times.pop(0)
            ft_duration.append(ft)
            # print(ft)
            while True:
                if len(failure_times) == 0:
                    break
                if int(ft+1) == int(failure_times[0]):
                    ft = failure_times.pop(0)
                    ft_duration.append(ft)
                else:
                    break

            if len(ft_duration) > 0:
                start_second = int(ft_duration[0]*60)
                end_second = int(ft_duration[-1]*60+59)
                server_type = server_dict[node_id][0]
                server_node_id_in_the_type = server_dict[node_id][1]
                unit_ft = f'{start_second} {end_second} {server_type} {node_id} {server_node_id_in_the_type}'
                try:
                    result[start_second].append(unit_ft)
                except :
                    result[start_second] = [unit_ft,]
    return result


def make_and_save_distribution(time_p, node_p, ds_config_file_location, base_trace):
    print(f'Start making failure trace file based on "{ds_config_file_location}" and "{base_trace}" distribution.')
    raw_total_servers, total_time, server_dict = get_node_num_server_num(ds_config_file_location)

    total_servers = int(raw_total_servers * node_p['failure_ratio'])
    total_time_min = int(total_time/60 + 1)
    pre_generated_lognormal = pre_generate_lognormal_time_distribution(time_p, total_time_min, total_servers)

    node_mean, node_stdev, node_d = get_lognormal_time_distribution(node_p, total_servers)
    result_list = list()
    node_d_dict = dict()
    total_failed_time = 0
    for node_i in range(total_servers):
        failed_time = int(total_time_min*node_d[node_i]/100)
        if failed_time > total_time_min:
            failed_time = total_time_min
        total_failed_time += failed_time
        pre_time_d = pre_generated_lognormal[random.randint(0, len(pre_generated_lognormal)-1)]
        node_d_dict[node_i] = sorted(pre_time_d[:failed_time])
    fta_dict = make_failure_trace_like_fta_format(server_dict, node_d_dict)

    result_list.append(f'#base_trace:{base_trace}, config_file:{ds_config_file_location}, '
                       f'total_servers:{raw_total_servers}, total_time:{total_time}, '
                       f'node_dist_mean:{node_mean}, node_dist_stdev:{node_stdev}')

    for t in sorted(fta_dict.keys()):
        fta_list = fta_dict[t]
        for fta in fta_list:
            result_list.append(fta)
    output_file = ds_config_file_location.replace('.xml', f'_{base_trace}_failure_time_distribution.txt')
    with open(output_file, 'w') as f:
        f.write('\n'.join(result_list))
    print(f'Ending making failure trace file based on "{ds_config_file_location}" and "{base_trace}" distribution.')
    print(f'Failure trace has been stored to "{output_file}"')
    print(f'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~')


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--base_trace', help='Base failure distribution [teragrid, nd07cpu, websites, batch_all]')
    parser.add_argument('--configs_xml', help='Location of ds-sim configuration xml file or directory of configs when using batch_all')
    args = parser.parse_args()
    base_trace = args.base_trace
    if base_trace == "teragrid":
        time_p = teragrid_time_p
        node_p = teragrid_node_p
    elif base_trace == "nd07cpu":
        time_p = nd07cpu_time_p
        node_p = nd07cpu_node_p
    elif base_trace == "websites02":
        time_p = websites02_time_p
        node_p = websites02_node_p
    elif base_trace == "batch_all":
        conf_dir = args.configs_xml
        if conf_dir[-1] != '/':
            conf_dir += '/'
        listOfFiles = os.listdir(conf_dir)
        pattern = "*.xml"
        for entry in listOfFiles:
            if fnmatch.fnmatch(entry, pattern):
                ds_config_file_location = f'{conf_dir}{entry}'
                make_and_save_distribution(websites02_time_p, websites02_node_p, ds_config_file_location, 'websites02')
                make_and_save_distribution(nd07cpu_time_p, nd07cpu_node_p, ds_config_file_location, 'nd07cpu')
                make_and_save_distribution(teragrid_time_p, teragrid_node_p, ds_config_file_location, 'teragrid')
    else:
        print('ERROR: NOT SUPPORTED TRACE TYPE: choose teragrid or nd07cpu or websites02 or batch_all')

    if base_trace != "batch_all":
        ds_config_file_location = args.configs_xml
        make_and_save_distribution(time_p, node_p, ds_config_file_location, base_trace)

